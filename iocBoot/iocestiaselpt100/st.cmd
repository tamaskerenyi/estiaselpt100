#!../../bin/linux-x86_64/estiaselpt100

#- You may have to change estiaselpt100 to something else
#- everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/estiaselpt100.dbd"
estiaselpt100_registerRecordDeviceDriver pdbbase

## Load record instances
#dbLoadRecords("db/xxx.db","user=tamaskerenyi")

cd "${TOP}/iocBoot/${IOC}"
iocInit

## Start any sequence programs
#seq sncxxx,"user=tamaskerenyi"
