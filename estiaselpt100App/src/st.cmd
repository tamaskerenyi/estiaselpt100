# Simple database to monitor ESTIA Selene PT100 temperature sensors        #
#                                                                          #
# created by: Tamas Kerenyi WP12                                           #
# tamas.kerenyi@esss.se                                                    #
#                                                                          #
# **************************************************************************

require modbus,3.0.0
require estiaselpt100,master

drvAsynIPPortConfigure("Bck","192.168.1.1:502",0,0,1)

modbusInterposeConfig("Bck",0,1000,0)

#drvModbusAsynConfigure(portName, 
#                       tcpPortName,
#                       slaveAddress, 
#                       modbusFunction, 
#                       modbusStartAddress, 
#                       modbusLength,
#                       dataType,
#                       pollMsec, 
#                       plcType);

drvModbusAsynConfigure(HoldingRegisterPort,Bck,0,4,0,32,0,1000,"Beckhoff");

### Load record instances

epicsEnvSet("EPICS_DB_INCLUDE_PATH", "$(estiaselpt100_DIR)db/")

## Load record instances

dbLoadRecords("estiaselpt100.db","P=MOD, PORT=modbus-stream, R=:, A=-1")

